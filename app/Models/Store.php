<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    use HasFactory;

    protected $table = 'stores';
    protected $primaryKey = 'store_id';

    protected $fillable = ['store_name','employee_name','email','mobile_no','address','state','city','pincode','status'];

    public $timestamps = true;
}
