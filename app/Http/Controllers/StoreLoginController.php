<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\Store;
use Hash;

class StoreLoginController extends Controller
{

    public function login(Request $request) {
       
        $details = Admin::where('email',$request->email)->first();
        
        if(!empty($details)){
        $password = $details['password'];
            if(Hash::check( $request->password , $password )) {
                $type_id = $details['type_id'];
                $store_details = Store::where('store_id',$type_id)->first();
               return response()->json(['success'=>true,'message'=>'login successful','data'=>$store_details['store_name'],'store_code'=>$store_details['store_code'],'email'=>$store_details['email'],'address'=>$store_details['address'],'mobile_no'=>$store_details['mobile_no'],
                    'state'=>$store_details['state'],
                    'city'=>$store_details['city'],
                    'pincode'=>$store_details['pincode']]);
            } else {
                return response()->json(['success'=>false,'message'=>'Invalid credential']);
            }
        } else {

            return response()->json(['success'=>false,'message'=>'Invalid credential']);
        }
    }

    public function logout(Request $request) {
    	return response()->json(['success'=>true,'message'=>'logout successful','is_store'=>'not_store','store_logout'=>"store_logout"]);
        
    }
}
