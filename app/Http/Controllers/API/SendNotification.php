<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SendNotification extends Controller
{
    public function sendwhatsappNotification(Request $request){
	$mobile_number = $request['mob_number'];
	$data = ['userId'=>$mobile_number,'whatsappOptIn'=>true];
	$data_encode = json_encode($data);

	$curl = curl_init();

	curl_setopt_array($curl, array(
 	CURLOPT_URL => 'https://api.webengage.com/v1/accounts/58add260/users',
  	CURLOPT_RETURNTRANSFER => true,
  	CURLOPT_ENCODING => '',
  	CURLOPT_MAXREDIRS => 10,
  	CURLOPT_TIMEOUT => 0,
  	CURLOPT_FOLLOWLOCATION => true,
  	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  	CURLOPT_CUSTOMREQUEST => 'POST',
  	CURLOPT_POSTFIELDS =>$data_encode,
  	CURLOPT_HTTPHEADER => array(
   	 	'Authorization: Bearer e5956b94-18a4-4eb4-a44a-05ee79402c4f',
   	 	'Content-Type: application/json'
  		),
	));	

	$response = curl_exec($curl);
	
	curl_close($curl);

	return response()->json(['success'=>true,'data'=>$response]);
    }
}

