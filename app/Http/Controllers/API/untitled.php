<?php

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header('Access-Control-Allow-Methods: GET, POST, PUT');
date_default_timezone_set('Asia/Kolkata');
ini_set("display_errors",1);
ini_set("log_errors",1);
ini_set("error_log",dirname(_FILE_).'/error_log.txt');
require dirname(_FILE_).'/vendor/autoload.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('name');
$log->pushHandler(new StreamHandler(dirname(_FILE_).'/logs.txt', Logger::WARNING));

// add records to the log
$log->warning('duroflex warning');
$log->error('duroflex error');

$webhook_content = "";
$webhook = fopen('php://input' , 'rb');
while (!feof($webhook)) {
    $webhook_content .= fread($webhook, 4096);
}
fclose($webhook);
//$log->warning($webhook_content);
$order_detail = json_decode($webhook_content,true);

//$order_id = $order_detail['id'];
//int_r($order_detail);
//$log->warning(print_r($order_detail,true));
$order_id = $order_detail['id'];
//$log->warning($order_id);
$shopify_url = "https://9ae72e683d6c0b882a249982f077d1d1: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@duroflex-world.myshopify.com/admin/api/2021-10/";
$order_url = $shopify_url . "orders/" . $order_id . ".json";

$order_detail_curl = curl_init();
curl_setopt_array($order_detail_curl, array(
    CURLOPT_URL => $order_url,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => false,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
            "Cookie: __cfduid=d8e72e86a356509a906b1db79a541edd81567512934"
    ),
));

$order_curl_response = curl_exec($order_detail_curl);
$order_detail_response = json_decode($order_curl_response,true);
curl_close($order_detail_curl);
//$log->warning(print_r($order_detail_response,true));

if(!empty($order_detail_response['order'])){
    if(!empty($order_detail_response['order']['customer'])){
            $date = date('d-m-Y', strtotime($order_detail_response['order']['created_at']));
            $user_data = ['order_id'=>$order_detail_response['order']['id'], 'mobile_no' => $order_detail_response['order']['shipping_address']['phone'], 'order_no' => $order_detail_response['order']['name'], 'cust_name' => $order_detail_response['order']['shipping_address']['name'], 'order_created_at' => $date];
            try{
                if($user_data['mobile_no'] != null){
                    $mobile = str_replace(' ', '', $user_data['mobile_no']);
                    $order_number = $user_data['order_no'];
                    $order_date = $user_data['order_created_at'];

                    $msg = "Hello, thank you for shopping with Duroflex. Your order ". $order_number ." has been successfully placed on ". $order_date.".";

                    $template_id = 1207163291849581734;

                    $order_url = "http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to=".$mobile."&msg=".urlencode($msg)."&msg_type=TEXT&userid=2000197613&auth_schema=plain&password=Duroflex@123&v=1.1&format=text";

                    $curl = curl_init();
                    curl_setopt_array($curl, array(
                        CURLOPT_URL => $order_url,
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 30,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => "POST",
                        CURLOPT_POSTFIELDS => "",
                        CURLOPT_SSL_VERIFYHOST => 0,
                        CURLOPT_SSL_VERIFYPEER => 0,
                    ));
                    $response = curl_exec($curl);
                    $err = curl_error($curl);
                    return [
                        'response' => $response,
                        'err' => $err
                    ];
                }
            }
            catch (Exception $e) {
                $log->error($e);
            }
        }
    }

?>