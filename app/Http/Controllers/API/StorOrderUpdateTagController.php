<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Store;
use App\Http\Traits\CurlHttp;
use Log;

class StorOrderUpdateTagController extends Controller
{
    use CurlHttp;
    public function orderTagUpdate (Request $request){
        $order_detail_response = $request->all();
        if(!empty($order_detail_response)){
            if(!empty($order_detail_response['customer'])){
                $date = date('d-m-Y', strtotime($order_detail_response['created_at']));
                $user_data = ['order_id'=>$order_detail_response['id'], 'mobile_no' => $order_detail_response['shipping_address']['phone'], 'order_no' => $order_detail_response['name'], 'cust_name' => $order_detail_response['shipping_address']['name'], 'order_created_at' => $date];
                try{
                    if($user_data['mobile_no'] != null){
                        $mobile = str_replace(' ', '', $user_data['mobile_no']);
                        $order_number = $user_data['order_no'];
                        $order_date = $user_data['order_created_at'];

                        $msg = "Hello, thank you for shopping with Duroflex. Your order ". $order_number ." has been successfully placed on ". $order_date.".";
                       
                
                        $template_id = 1207163291849581734;

                        $order_url = "http://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to=".$mobile."&msg=".urlencode($msg)."&msg_type=TEXT&userid=2000197613&auth_schema=plain&password=Duroflex@123&v=1.1&format=text";
                        $curl = curl_init();
                                            
                        curl_setopt_array($curl, array(
                            CURLOPT_URL => $order_url,
                            CURLOPT_RETURNTRANSFER => true,
                            CURLOPT_ENCODING => "",
                            CURLOPT_MAXREDIRS => 10,
                            CURLOPT_TIMEOUT => 30,
                            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                            CURLOPT_CUSTOMREQUEST => "POST",
                            CURLOPT_POSTFIELDS => "",
                            CURLOPT_SSL_VERIFYHOST => 0,
                            CURLOPT_SSL_VERIFYPEER => 0,
                        ));
                        $response = curl_exec($curl);
			$store_coco = '';
			$store_code = '';					
			if (isset($order_detail_response['note_attributes'])) {
                        foreach ($order_detail_response['note_attributes'] as $key => $note_attributes){
                            if($note_attributes['name'] == 'store_attribute'){
				$str_replace = str_replace("=>",":",$note_attributes['value']);
                                $json_decode = json_decode($str_replace,true);
                                $store_coco = $json_decode['store_name'];
                                $store_code = $json_decode['store_code']; 
                                $url = '/admin/api/2019-04/orders/'.$order_detail_response['id'].'.json';
                                $tags = 'Store_Order';
	                        $order_array = [
				    'order' =>[
                                        'id'    =>  $order_detail_response['id'],
                                        'tags'  =>  $store_coco.','.$store_code.','.$tags,
                                    ]
                                ];    
                                $response_data=$this->putCurl($url,$order_array);
                            }
                        }
                    }
		Log::info('order_mesage'.$order_detail_response['id']);
		Log::info($response);
                        $err = curl_error($curl);
                        return [
                            'response' => $response,
                            'err' => $err
                        ];
                    }
                }
                catch (Exception $e) {
                    $log->error($e);
                }
            }
        }
    }
}
