<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin;
use auth,Hash;


class PasswordController extends Controller
{
    //
     public function changed()
    {
        return view('admin.password_change');
    }

    public function postChanged(Request $request){

        $password = auth('admin')->user()->password;
            
        if (!Hash::check($request->current_password, $password)) {
            return redirect()->back()->withErrors(['current_password' => 'Current password is not correct']);
        }else{
            $email = [];
    
            $user_data = Admin::where('email',auth('admin')->user()->email)->first();
            
            $admin = Admin::find($user_data['id']);
            $admin->password = Hash::make($request->password); 
            $admin->password_changed_at = now();
            $admin->Save();
        
            auth('admin')->logout();
            return redirect()->route('admin.login')->with('message','Your Password has been successfully changed')->with('message_type','success');  
                
        }
    }
}
