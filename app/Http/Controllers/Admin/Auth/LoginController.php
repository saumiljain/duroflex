<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\User;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = RouteServiceProvider::HOME;
    protected $redirectTo = 'stores';
    protected function redirectTo()
    {
        if (Auth::guard() == 'admin') {
            return '/stores';
        }
        return '/admin/login';
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        return view('admin.login', ['url' => 'admin']);
    }

    protected function guard()
    {
        return Auth::guard('admin');
    }

    public function login(Request $request)
    {

        $request->validate([
                'email' => 'required',
                'password' => 'required'
            ]);
        if(is_numeric($request->get('email'))) {

            if (Auth::guard('admin')->attempt(['mobile' => $request->email, 'password' => $request->password])) {

                return redirect()->route('stores.index')->with('success','Login successfully.')->with('message_type','success');
            }
        } elseif(filter_var($request->get('email'), FILTER_VALIDATE_EMAIL)) {

            if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password])) {

                return redirect()->route('stores.index')->with('success','Login successfully.')->with('message_type','success');
            }
        }


        return redirect()->route('admin.login')->with('error','These credentials do not match our records.')->with('message_type','error');
    }

    public function logout () {

        Session::flush();
        Auth::guard('admin')->logout();

        return redirect()->route('admin.login')->with('success','Logout successfully.')->with('message_type','success');
    }
}
