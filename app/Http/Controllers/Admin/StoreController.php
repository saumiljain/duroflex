<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\StoreRequest;
use App\Models\Store;
use App\Models\Admin;
use Hash;

class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $type_id = auth('admin')->user()->type_id;
        if($request->ajax()){

            $where_str    = "1 = ?";
            $where_params = array(1);
            $status_type = $request->get('type');

            if (!empty($request->input('sSearch')))
            {
                $search     = $request->input('sSearch');
                $where_str .= " and (store_code like \"%{$search}%\""
                . " or store_name like \"%{$search}%\""
                . " or email like \"%{$search}%\""
                . " or mobile_no like \"%{$search}%\""
                . ")";
            }   

            if($status_type == 'inactive')
            {
                $status = 0;
                $where_str.= " and (status =$status)";
            }
            elseif($status_type == 'active')
            {
                $status = 1;
                $where_str.= " and (status =$status)";
            }
                                         

            $columns = ['store_id','store_code','store_name','email','mobile_no','status'];
            
/*            if (!empty($request->input('sony_center_wise')))
            {
                $sony_center_wise=$request->input('sony_center_wise');
                $where_str .= " And (sony_center_name ='$sony_center_wise')";
            }*/

            if($type_id == 0 ){

                $store_count = Store::select($columns)
                 ->whereRaw($where_str, $where_params)->count();

                 $store_list = Store::select($columns)
                 ->whereRaw($where_str, $where_params);

                
            }else{

                $store_count = Store::select($columns)
                ->where('store_code',$type_id)
                ->whereRaw($where_str, $where_params)->count();

                $store_list = Store::select($columns)
                ->where('store_code',$type_id)
                ->whereRaw($where_str, $where_params);

            }

            if($request->get('iDisplayStart') != '' && $request->get('iDisplayLength') != ''){
                $store_list = $store_list->take($request->input('iDisplayLength'))
                ->skip($request->input('iDisplayStart'));
            }
                    
            if($request->input('iSortCol_0')){
                $sql_order='';
                for ( $i = 0; $i < $request->input('iSortingCols'); $i++ )
                {
                    $column = $columns[$request->input('iSortCol_' . $i)];
                    if(false !== ($index = strpos($column, ' as '))){
                        $column = substr($column, 0, $index);
                    }
                    $store_list = $store_list->orderBy($column,$request->input('sSortDir_'.$i));   
                }
            }
          
            $store_list = $store_list->get();
            $response['iTotalDisplayRecords'] = $store_count;
            $response['iTotalRecords'] = $store_count;
            $response['sEcho'] = intval($request->input('sEcho'));
            $response['aaData'] = $store_list->toArray();

            

            return $response;
        }
       
        // $filter_sony_center =[""=>"Select Sony center"]+QuaterlyTcsReportData::select('sony_center_name')->distinct()->pluck('sony_center_name','sony_center_name')->toArray();
        // $path = Storage::path('public').'/tcs/';

        return view('admin.store.index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        return view('admin.store.add');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        //
        
        $store_details = new Store();
	$store_details->store_code = $request->store_code;
        $store_details->store_name = $request->store_name;
        $store_details->employee_name = $request->employee_name;
        $store_details->email = $request->email;
        $store_details->mobile_no = $request->mobile_no;
        $store_details->address = $request->address;
        $store_details->state = $request->state;
        $store_details->city = $request->city;
        $store_details->pincode = $request->pincode;
        $store_details->save();
        $store_id = $store_details->store_id;

        $admin_details = new Admin();
        $admin_details->type_id = $store_id;
        $admin_details->name = $request->employee_name;
        $admin_details->email = $request->email;
        $admin_details->mobile = $request->mobile_no;
        $admin_details->password = Hash::make($request->password);
        $admin_details->save();

        return redirect()->route('stores.index')->with('success','Store create successfully.')->with('message_type','success');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $store_data = Store::where('store_id',$id)->first();
        return view('admin.store.edit',compact('store_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function add() {

        
    }

    public function updateData(Request $request) {

        $store_id = $request->store_id;
        $store_details = Store::find($store_id);
	$store_details->store_code = $request->store_code;
        $store_details->store_name = $request->store_name;
        $store_details->email = $request->email;
        $store_details->employee_name = $request->employee_name;
        $store_details->mobile_no = $request->mobile_no;
        $store_details->address = $request->address;
        $store_details->state = $request->state;
        $store_details->city = $request->city;
        $store_details->pincode = $request->pincode;
        $store_details->status = $request->status;
        $store_details->save();

        return redirect()->route('stores.index')->with('success','Store Update successfully.')->with('message_type','success');
        
    }
}
