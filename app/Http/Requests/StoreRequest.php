<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request) 
    {
        
        $store_id = $request->store_id;
        if(empty($store_id)){

            return [
                
                'store_name' => 'required',
                'employee_name' => 'required|min:2',
                'email' => 'required|email|unique:stores|regex:/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix',
                'mobile_no' =>'required|digits:10|numeric|unique:stores',
                'password' =>  'required|min:6|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
                'address' => 'required',
                'state' => 'required|alpha',
                'city' => 'required|alpha',
                'pincode' => 'required|digits:6|numeric',
            ];
        } else {

            return [
                
                'store_name' => 'required',
                'employee_name' => 'required|min:2',
                'mobile_no' =>'required|digits:10|numeric|unique:stores,mobile_no,'.$this->store_id,
                'address' => 'required',
                'state' => 'required|alpha',
                'city' => 'required|alpha',
                'pincode' => 'required|digits:6|numeric',
            ];
        }
    }

    public function messages() {

        return [
            'store_name.required' => 'Storecoco is required',

            'employee_name.required' => 'Employee name is required',
            'employee_name.min' => 'Please enter valid employee name',

            'email.required' => 'Email is required',
            'email.email' => 'Please enter valid email',
            'email.unique' => 'Emailid already exists',
            'email.regex' => 'Please enter valid email',

            'mobile_no.required' => 'Mobile number is required',
            'mobile_no.numeric' => 'The :attribute must be a number',
            'mobile_no.digit' => 'The :attribute must be :digits digits',
            'mobile_no:unique' => 'This mobile number is already registered',

            'password.required' => 'Password is required',
            'password.min' => 'The :attribute may not be less than :min characters.',
            'password.regex' => 'Your password must be have 1 upercase leter 1 lowercase leter 1 degit & 1 special char',

            'address.required' => 'Address is required',

            'state.required' => 'State is required',
            'state.alpha' => 'Enter only alphabetic value',

            'city.required' => 'City is required',
            'city.alpha' => 'Enter only alphabetic value',

            'pincode.required' => 'Mobile number is required',
            'pincode.numeric' => 'The :attribute must be a number',
            'pincode.digit' => 'The :attribute must be :digits digits',
            
        ];

    }
}
