<?php

namespace App\Http\Traits;

use Config;
use Illuminate\Http\Request;
use Log;
use App\Models\User;
use Response;
use URL;
trait CurlHttp {

	public function putCurl($url,$data){

		$shop_data = User::select('name','password')->first();
		$response = $shop_data->api()->rest('PUT',$url,$data);

		$response=json_encode($response['body']);
		$response = json_decode($response,true);
		return $response;
	}

}