<svg xmlns="http://www.w3.org/2000/svg" width="20.472" height="24.074" viewBox="0 0 20.472 24.074">
  <path id="path696" d="M18.912,10.2v3.453a7.049,7.049,0,1,1-6.618-.039v-3.4a10.27,10.27,0,0,0-6.723,7.616,10.153,10.153,0,0,0-.079,3.624,10.234,10.234,0,1,0,13.472-11.24c-.018-.006-.034-.008-.053-.013Z" transform="translate(-5.366 -6.061)" fill-rule="evenodd"/>
  <rect id="Rectangle_8" data-name="Rectangle 8" width="4" height="11" rx="2" transform="translate(8.236)" class="svgover"/>
</svg>