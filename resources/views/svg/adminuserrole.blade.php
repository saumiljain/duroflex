<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
  <g id="Group_21" data-name="Group 21" transform="translate(-104.036 -1224.58)">
    <g id="user_1_" data-name="user(1)" transform="translate(109.666 1231.35)">
      <g id="Group_18" data-name="Group 18">
        <path id="Path_164" data-name="Path 164" d="M165.792,168.312a3.086,3.086,0,1,0-3.086-3.086A3.088,3.088,0,0,0,165.792,168.312Zm0-5.133a2.046,2.046,0,1,1-2.046,2.046A2.05,2.05,0,0,1,165.792,163.179Z" transform="translate(-161.423 -162.139)"/>
        <path id="Path_205" data-name="Path 205" d="M137.476,324.939a3.924,3.924,0,0,0-3.138,1.712,6.587,6.587,0,0,0-1.231,3.94.522.522,0,0,0,.52.52h7.7a.522.522,0,0,0,.52-.52,6.587,6.587,0,0,0-1.231-3.94A3.924,3.924,0,0,0,137.476,324.939Zm-3.308,5.133c.186-2.3,1.6-4.092,3.308-4.092s3.121,1.795,3.308,4.092Z" transform="translate(-133.106 -317.882)" />
      </g>
    </g>
    <g id="user_1_2" data-name="user(1)" transform="translate(104.036 1224.58)">
      <g id="Group_18-2" data-name="Group 18" transform="translate(0 0)">
        <path id="Path_165" data-name="Path 165" d="M118.346,326.559a.533.533,0,0,0-.152-.368.52.52,0,1,0,0,.737A.541.541,0,0,0,118.346,326.559Z" transform="translate(-112.295 -312.164)" class="svgover"/>
        <path id="Path_166" data-name="Path 166" d="M199.182,76.238a.519.519,0,1,0-.512-.438A.517.517,0,0,0,199.182,76.238Z" transform="translate(-190.178 -72.008)" class="svgover"/>
        <path id="Path_167" data-name="Path 167" d="M245.567,75.106a.52.52,0,1,0-.078,1.032A.517.517,0,0,0,246,75.7.523.523,0,0,0,245.567,75.106Z" transform="translate(-234.502 -71.912)" class="svgover"/>
        <path id="Path_168" data-name="Path 168" d="M369.2,244.9a.533.533,0,1,0-.512.438A.516.516,0,0,0,369.2,244.9Z" transform="translate(-352.439 -233.884)" class="svgover"/>
        <path id="Path_169" data-name="Path 169" d="M368.506,198a.522.522,0,0,0-.433.594.517.517,0,0,0,.511.438c.026,0,.056,0,.082,0a.52.52,0,0,0-.16-1.027Z" transform="translate(-352.343 -189.559)" class="svgover"/>
        <path id="Path_170" data-name="Path 170" d="M117.7,117.755a.529.529,0,0,0,.368-.152.521.521,0,1,0-.737,0A.529.529,0,0,0,117.7,117.755Z" transform="translate(-112.175 -111.748)" class="svgover"/>
        <path id="Path_171" data-name="Path 171" d="M354.212,154.965a.534.534,0,0,0,.238-.056.523.523,0,1,0-.238.056Z" transform="translate(-338.581 -147.369)" class="svgover"/>
        <path id="Path_172" data-name="Path 172" d="M354.648,288.383a.52.52,0,0,0-.468.928.5.5,0,0,0,.234.056.52.52,0,0,0,.234-.984Z" transform="translate(-338.775 -276.03)" class="svgover"/>
        <path id="Path_173" data-name="Path 173" d="M326.9,117.555a.52.52,0,1,0-.368-.152A.522.522,0,0,0,326.9,117.555Z" transform="translate(-312.438 -111.557)" class="svgover"/>
        <path id="Path_174" data-name="Path 174" d="M76.8,244.939h0a.52.52,0,0,0-1.027.16.517.517,0,0,0,.512.438c.026,0,.056,0,.082,0A.52.52,0,0,0,76.8,244.939Z" transform="translate(-72.53 -234.075)" class="svgover"/>
        <path id="Path_175" data-name="Path 175" d="M154.973,89.539a.518.518,0,0,0-.225.7.512.512,0,0,0,.464.282.534.534,0,0,0,.238-.056.52.52,0,1,0-.477-.923Z" transform="translate(-148.083 -85.678)" class="svgover"/>
        <path id="Path_176" data-name="Path 176" d="M90.048,154.464a.519.519,0,1,0,.7-.225A.518.518,0,0,0,90.048,154.464Z" transform="translate(-86.147 -147.614)" class="svgover"/>
        <path id="Path_177" data-name="Path 177" d="M76.106,199.233a.525.525,0,0,0,.082,0,.519.519,0,0,0,.078-1.032.52.52,0,1,0-.16,1.027Z" transform="translate(-72.434 -189.753)" class="svgover"/>
        <path id="Path_178" data-name="Path 178" d="M289.748,89.381a.518.518,0,1,0-.468.923.5.5,0,0,0,.234.056.518.518,0,0,0,.234-.98Z" transform="translate(-276.648 -85.528)" class="svgover"/>
        <path id="Path_179" data-name="Path 179" d="M91.071,288.906a.519.519,0,1,0-.46.754.5.5,0,0,0,.234-.056.518.518,0,0,0,.225-.7Z" transform="translate(-86.242 -276.315)" class="svgover"/>
        <path id="Path_180" data-name="Path 180" d="M327.026,325.939a.533.533,0,0,0-.368.152.523.523,0,1,0,.368-.152Z" transform="translate(-312.558 -312.069)" class="svgover"/>
        <path id="Path_181" data-name="Path 181" d="M65.426,378.339a.521.521,0,1,0,.52.52.533.533,0,0,0-.152-.369A.525.525,0,0,0,65.426,378.339Z" transform="translate(-62.092 -362.197)" class="svgover"/>
        <path id="Path_182" data-name="Path 182" d="M441.443,258.778a.519.519,0,1,0-.173,1.023.645.645,0,0,0,.087.009.52.52,0,0,0,.087-1.032Z" transform="translate(-422.007 -247.736)" class="svgover"/>
        <path id="Path_183" data-name="Path 183" d="M76.426,55.079a.52.52,0,1,0-.39-.178A.52.52,0,0,0,76.426,55.079Z" transform="translate(-72.663 -51.716)" class="svgover"/>
        <path id="Path_184" data-name="Path 184" d="M399.277,88.13a.542.542,0,0,0,.316-.1.524.524,0,1,0-.316.1Z" transform="translate(-381.753 -83.392)" class="svgover"/>
        <path id="Path_185" data-name="Path 185" d="M113.792,28.824a.52.52,0,1,0-.512-.906.516.516,0,0,0-.2.707.522.522,0,0,0,.455.264A.543.543,0,0,0,113.792,28.824Z" transform="translate(-108.188 -26.663)" class="svgover"/>
        <path id="Path_186" data-name="Path 186" d="M443.794,214.071a.518.518,0,0,0,.52.5h.017a.52.52,0,1,0-.035-1.04A.514.514,0,0,0,443.794,214.071Z" transform="translate(-424.835 -204.432)" class="svgover"/>
        <path id="Path_187" data-name="Path 187" d="M422.369,126.441a.523.523,0,0,0,.468.295.534.534,0,0,0,.225-.052.521.521,0,1,0-.694-.243Z" transform="translate(-404.29 -120.345)" class="svgover"/>
        <path id="Path_188" data-name="Path 188" d="M437.6,169.226a.519.519,0,0,0,.507.4.578.578,0,0,0,.121-.013.52.52,0,1,0-.629-.386Z" transform="translate(-418.896 -161.405)" class="svgover"/>
        <path id="Path_189" data-name="Path 189" d="M331.316,27.742a.519.519,0,0,0-.507.906.547.547,0,0,0,.256.065.517.517,0,0,0,.251-.971Z" transform="translate(-316.421 -26.495)" class="svgover"/>
        <path id="Path_190" data-name="Path 190" d="M6.55,169.716a.61.61,0,0,0,.121.013.521.521,0,1,0-.5-.646A.52.52,0,0,0,6.55,169.716Z" transform="translate(-5.886 -161.5)" class="svgover"/>
        <path id="Path_191" data-name="Path 191" d="M4.168,259.5a.519.519,0,1,0-.512.611.534.534,0,0,0,.087-.009A.519.519,0,0,0,4.168,259.5Z" transform="translate(-3.001 -248.024)" class="svgover"/>
        <path id="Path_192" data-name="Path 192" d="M36.539,343.2a.52.52,0,0,0,.286.954.5.5,0,0,0,.286-.087.519.519,0,0,0-.572-.867Z" transform="translate(-34.731 -328.497)" class="svgover"/>
        <path id="Path_193" data-name="Path 193" d="M21.621,126.984a.52.52,0,0,0,.442-.941.52.52,0,1,0-.442.941Z" transform="translate(-20.398 -120.632)" class="svgover"/>
        <path id="Path_194" data-name="Path 194" d="M16.287,303.144a.522.522,0,1,0-.486.711.57.57,0,0,0,.191-.035A.525.525,0,0,0,16.287,303.144Z" transform="translate(-14.618 -289.897)" class="svgover"/>
        <path id="Path_195" data-name="Path 195" d="M.5,214.874H.521a.512.512,0,1,0-.017,0Z" transform="translate(0 -204.719)" class="svgover"/>
        <path id="Path_196" data-name="Path 196" d="M289.254,10.593a.491.491,0,0,0,.156.026.522.522,0,1,0-.5-.681A.527.527,0,0,0,289.254,10.593Z" transform="translate(-276.548 -9.181)" class="svgover"/>
        <path id="Path_197" data-name="Path 197" d="M244.9,1.606c.017,0,.035,0,.052,0a.536.536,0,1,0-.052,0Z" transform="translate(-233.996 -0.567)" class="svgover"/>
        <path id="Path_198" data-name="Path 198" d="M199.461,1.515a.211.211,0,0,0,.052,0,.506.506,0,1,0-.052,0Z" transform="translate(-190.44 -0.471)" class="svgover"/>
        <path id="Path_199" data-name="Path 199" d="M155.066,10.719a.491.491,0,0,0,.156-.026.521.521,0,1,0-.655-.338A.528.528,0,0,0,155.066,10.719Z" transform="translate(-147.941 -9.277)" class="svgover"/>
        <path id="Path_200" data-name="Path 200" d="M45.165,88.226a.524.524,0,0,0,.728-.1.52.52,0,0,0-.828-.629h0A.519.519,0,0,0,45.165,88.226Z" transform="translate(-43.01 -83.583)" class="svgover"/>
        <path id="Path_201" data-name="Path 201" d="M408.558,342.971a.52.52,0,1,0-.568.871.527.527,0,0,0,.286.087.512.512,0,0,0,.433-.234A.527.527,0,0,0,408.558,342.971Z" transform="translate(-390.357 -328.282)" class="svgover"/>
        <path id="Path_202" data-name="Path 202" d="M367.845,54.929h0a.52.52,0,0,0,.342.13.531.531,0,0,0,.395-.178.519.519,0,0,0-.785-.681A.508.508,0,0,0,367.845,54.929Z" transform="translate(-351.958 -51.701)" class="svgover"/>
        <path id="Path_203" data-name="Path 203" d="M429.292,302.549a.522.522,0,0,0-.381.971.48.48,0,0,0,.191.035.521.521,0,0,0,.191-1.006Z" transform="translate(-410.281 -289.61)" class="svgover"/>
        <path id="Path_204" data-name="Path 204" d="M379.426,378.339a.518.518,0,1,0,.368.152A.541.541,0,0,0,379.426,378.339Z" transform="translate(-362.76 -362.197)" class="svgover"/>
      </g>
    </g>
  </g>
</svg>