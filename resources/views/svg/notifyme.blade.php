<svg xmlns="http://www.w3.org/2000/svg" width="19.5" height="20" viewBox="0 0 19.5 20">
  <g id="Group_41" data-name="Group 41" transform="translate(-1398.999 -554.001)">
    <path id="Subtraction_14" data-name="Subtraction 14" d="M7.712,20a2.044,2.044,0,0,1-2.033-2.051H9.746A2.045,2.045,0,0,1,7.712,20ZM14.4,16.923H1.014a1,1,0,0,1-.927-.615A1.02,1.02,0,0,1,.3,15.169l1.312-1.323V8.717A7.325,7.325,0,0,1,2.807,4.553,5.61,5.61,0,0,1,6.188,2.236v-.7a1.524,1.524,0,1,1,3.049,0v.518A5.9,5.9,0,0,0,8.133,5.5a6.055,6.055,0,0,0,5.678,5.985v2.36l1.311,1.323a1.03,1.03,0,0,1-.722,1.754Z" transform="translate(1398.999 554.001)" fill="#a8a8a8"/>
    <circle id="Ellipse_12" data-name="Ellipse 12" cx="5" cy="5" r="5" transform="translate(1408.499 554.001)" class="svgover"/>
  </g>
</svg>