<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
  <g id="Group_22" data-name="Group 22" transform="translate(-106 -158.328)">
    <g id="Icons_Renewed_2-02" data-name="Icons Renewed_2-02" transform="translate(106 158.328)">
      <path id="Path_61" data-name="Path 61" d="M17.272,14.427v2.49L9.538,19.152V3.662l2.841.618a6.109,6.109,0,0,1,1.083-1.822L8.856,1.609H8.8a.9.9,0,0,0-.335,0H8.314l-.966.306-1.92.629V7.871l-2.136.445V3.25l-.875.618h0l-.788.26A1.085,1.085,0,0,0,1,5.116V17.687a1.077,1.077,0,0,0,.669.987l6.648,2.907a.77.77,0,0,0,.276,0,.752.752,0,0,0,.266,0l9.634-2.864a1.077,1.077,0,0,0,.669-.987V14.048a4.911,4.911,0,0,1-1.871.38Z" transform="translate(-1 -1.594)" fill="#a8a8a8"/>
    </g>
    <g id="Icons_Renewed_2-03" data-name="Icons Renewed_2-03" transform="translate(116.099 160.07)">
      <path id="Path_65" data-name="Path 65" d="M17.744,2.19A5.035,5.035,0,0,0,12.78,7.3a4.954,4.954,0,1,0,9.9.009A5.036,5.036,0,0,0,17.744,2.19Zm.315,6.237-.96.964-.933-.978-.919-.935.96-.978.905.935,2.181-2.239.933.992Z" transform="translate(-12.78 -2.19)"  class="svgover"/>
    </g>
  </g>
</svg>