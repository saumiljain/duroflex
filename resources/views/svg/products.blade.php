<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
  <g id="Icons_Renewed_2-10" data-name="Icons Renewed_2-10" transform="translate(0.028 -4.07)">
    <path id="Path_87" data-name="Path 87" d="M9.987,11.36V22.61l6.55-1.25v-7a.28.28,0,0,1,.31-.28l.809.12a.29.29,0,0,1,.25.28v7.67a.28.28,0,0,1-.22.27L10.477,24a2.472,2.472,0,0,1-1.048,0L2.339,22.5a.29.29,0,0,1-.23-.27L2,14.46a.28.28,0,0,1,.26-.29l5.991-.53a.29.29,0,0,0,.24-.17L9.4,11.26a.3.3,0,1,1,.589.1Z" transform="translate(0 0.014)" />
    <path id="Path_88" data-name="Path 88" d="M1.92,5.67,0,11.71a.28.28,0,0,0,.28.4l7.14-.71a.29.29,0,0,0,.23-.17L9.94,4.07,2.13,5.51a.29.29,0,0,0-.21.16Z" class="svgover" />
    <path id="Path_89" data-name="Path 89" d="M9.94,4.07l2.42,7.32a.27.27,0,0,0,.24.16l7.1.58a.28.28,0,0,0,.27-.4L18.12,5.67a.27.27,0,0,0-.2-.16Z" transform="translate(-0.025)" class="svgover" />
  </g>
</svg>