@extends('admin.layout.layout')
@section('style')
@stop
@section('header')
<nav class="navbar navbar-expand-xl navbar-light fixed-top hk-navbar">
    <a id="navbar_toggle_btn" class="navbar-toggle-btn nav-link-hover" href="javascript:void(0);"><span
            class="feather-icon"><i data-feather="menu"></i></span></a>
    <div class="container">        
        <div class="hk-pg-header col-md-6">
            <h4 class="hk-pg-title"><span class="pg-title-icon"><span class="feather-icon"></span></span>{{ __('Orders') }}</h4>
        </div>
   </div>
</nav>
@stop
@section('content')
<div class="container">
    <div class="row">
        <div class="col-xl-12">
            <section class="hk-sec-wrapper">
                <div class="row">
                    <div class="col-sm-12">
                        <table id="master_table" class="table table-hover w-100 display">
                            <thead>
                                <tr>
                                    <th class="tdcheckbox"></th>
                                    <th class="tddate">ORDER DATE</th>
                                    <th class="tdorder">ORDER NO</th>
                                    <th class="tdcustomer">CUSTOMER NAME</th>
                                    <th class="tdemailid">EMAIL ID</th>
                                    <th class="thprice">PRICE</th>
                                    <!-- <th class="tdstatus">STATUS</th> -->
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
@stop
@section('script')
{{  Html::script('backend/js/jquery.form.min.js', []) }}
{{  Html::script('backend/js/moment.min.js', []) }}
{{  Html::script('backend/js/daterangepicker.js', []) }}
{{  Html::script('backend/js/jquery.dataTables.min.js',[])  }}
{{  Html::script('backend/js/dataTables.bootstrap4.min.js',[])  }}
{{  Html::script('backend/js/dataTables.dataTables.min.js',[])  }}
{{  Html::script('backend/js/fnstandrow.js', []) }}
{{  Html::script('backend/js/nprogress.js')  }}
{{  Html::script('backend/js/handlebars-v4.0.12.js',[])  }}

<!-- AJAX FOR DATA TABLE  START-->
<script type="text/javascript">
    var url = "{{URL::route('orders.index')}}";
    $(document).ready(function() {
        $('.select2').select2();
    });
       var order_table = $('#master_table').DataTable({
            "bProcessing": true,
            "bServerSide": true,
            "autoWidth": true,
            "aaSorting": [
                [1, "desc"]
            ],
            lengthMenu: [
                [50, 100, 200, 500],
                ['50', '100', '200', '500']
            ],
            "fnServerParams": function(aoData) {},
            "sAjaxSource": url,
            "aoColumns": [
                {
                    "className": 'tdcheckbox details-control',
                    "orderable": false,
                    "searchable": false,
                    "data": null,
                    "defaultContent": ''
                },
                {
                "mData": "created_at", "className": "tddate", bSortable: true, bVisible: true,
                mRender: function (v, t, o) {
                    var expected_date = '-';
                    var date = o['created_at'];
                    if (date != '0000-00-00' && date != null) {
                        var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                        date = moment(date).format('YYYY-MM-DD HH:mm:ss');
                        var d = new Date(date.split("-"));
                        var dd = d.getDate();
                        var mm = months[d.getMonth()];
                        var yy = d.getFullYear();
                        var dt = new Date(date);
                        var hours = dt.getHours() ;
                        console.log(hours)
                        var AmOrPm = hours >= 12 ? 'PM' : 'AM';
                        hours = (hours % 12) || 12;
                        var minutes = dt.getMinutes() ;
                        var finalTime =  dd + " " + mm + ", " + yy +" " + hours + ":" + minutes + " " + AmOrPm;
                        var change_date = finalTime;
                    } else {
                        var change_date = '-';
                    }
                    return change_date;
                }
                },
                {
                    "mData": "order_number", "className": "tdorder", bSortable: true,
                },
                { "mData": "customer_name", bSortable: true, "className": "tdcustomer", bVisible: true },
                { "mData": "customer_email", bSortable: true, "className": "tdemailid", bVisible: true },
                { "mData": "total_price", bSortable: true, "className": "thprice", bVisible: true },
                { "mData": "fulfillment_status", bSortable: true, "className": "tdstatus", bVisible: false },
            ],
            fnPreDrawCallback: function () {
                NProgress.start();
            },
            fnDrawCallback: function (oSettings) {
                NProgress.done();
            }
        });
    
</script>
<!-- AJAX FOR DATA TABLE  END-->
@include('admin.order.order_item_display')
@include('admin.layout.alert')
@stop