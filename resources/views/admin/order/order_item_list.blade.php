<table class="table details-table table-hover w-100 display dataTable no-footer lineitem_data">
    <thead>
        <tr>
            <th class="text-center">SKU</th>
            <th class="text-center">TITLE</th>
            <th class="text-center">ORDER QTY</th>
            <th class="text-right">PRICE</th>
        </tr>
    </thead>
    <tbody>
         @foreach($variant_data as $single_variant_data)
        <tr>
            <td class="text-center">{{ $single_variant_data['sku'] }}</td>
            <td class="text-center">{{ $single_variant_data['title'] }}</td>
            <td class="text-center">{{ $single_variant_data['quantity'] }}</td>
            <td class="text-right">{{ $single_variant_data['price'] }}</td>
        </tr>
        @endforeach
    </tbody>
</table>

