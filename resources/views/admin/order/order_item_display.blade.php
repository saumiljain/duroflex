<script type="text/javascript">
    $('#master_table tbody').on('click', 'td.details-control', function () {
        var template = Handlebars.compile($("#details-template").html());
        var tr = $(this).closest('tr');
        var row = order_table.row(tr);
        var tableId = 'orders-' + row.data().id;

        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            var data = row.data();
            // Open this row
            row.child(template(row.data())).show();
            initTable(tableId, row.data());
            tr.addClass('shown');
            tr.next().find('td').addClass('no-padding bg-gray');
        }
    });
    var return_html = '';

    function initTable(tableId, data) {

        var order_id = data.id;
        var url = "{{ URL::route('order.details',['id'=>':id']) }}";
        url = url.replace(':id',order_id);
        var checkbox_status = true;

        $.ajax({
            type: 'POST',
            url: url,
            data:{'_token': '{{csrf_token()}}'},
            beforeSend:function(){
                $('.preloader-it').show();
            },

            success: function (res) {
                $('.preloader-it').hide();
                if (res.success == true) {
                    if(res.view_render != ""){

                        $('.sub_order_'+order_id).html(res.view_render);
                    }else{
                        $('.sub_order_'+order_id).html("No Data available.");
                    }
                }
            },
            errors: function(resError){
                console.log(resError);
            }
        });
    }
</script>

<script id="details-template" type="text/x-handlebars-template">
    <div class="sub_order_@{{id}}">
    </div>
</script>
@include('admin.layout.alert')
