<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>Duroflex Reset Password</title>
    <meta name="description" content="A responsive bootstrap 4 admin dashboard template by hencework" />

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <style type="text/css">
        .passcss{
            display: flex; 
            margin-top: 360px;
            margin-left: -440px;
            font-size: 20px
        }
        .divflex{
            display: flex;
        }
    </style>
    <!-- Custom CSS -->
    {{ Html::style('backend/css/style.css',[]) }}
    {{ Html::style('backend/css/toster.min.css') }}

</head>

<body>
    <!-- Preloader -->
    <div class="preloader-it">
        <div class="loader-pendulums"></div>
    </div>
    <!-- /Preloader -->
	<!-- HK Wrapper -->
	<div class="hk-wrapper">
        <!-- Main Content -->
        <div class="hk-pg-wrapper hk-auth-wrapper divflex">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-12 pa-0">
                        <div class="auth-form-wrap pt-xl-0 pt-70">
                            <div class="auth-form w-xl-30 w-sm-50 w-100">
                                <a class="auth-brand text-center d-block mb-20" href="#">
                                    
                                </a>
                                {{ Form::open(['route' => 'password.post.changed','class'=>'form-signin','method'=>'POST','onsubmit'=>'return validate();']) }}

                                    <input type="hidden" name="token" value="{{csrf_token()}}">
                                    <h3 class="text-center">Please change your password</h3>
                                    <p class="text-center">A password must contains at least 4 characters, including at least one number and includes both lower and uppercase letters and special characters</p><br>
                                    <div class="form-group">
                                        {{Form::password('current_password', ['class' => 'form-control mb-0','id'=>'current_password', 'placeholder' => 'Enter your Current password'])}}
                                        <span id="current_password_error" class="color-w" style="color:red;">{{$errors->first('current_password')}}</span>
                                    </div>
                                    <div class="form-group">
                                        {{Form::password('password', ['class' => 'form-control mb-0','id'=>'password', 'placeholder' => 'Enter your new password'])}}
                                        <span id="password_error" class="color-w" style="color:red;">{{$errors->first('password')}}</span>
                                    </div>
                                    <div class="form-group">
                                        {{Form::password('password_confirmation', ['class' => 'form-control mb-0','id'=>'password_confirmation', 'placeholder' => 'Enter password again'])}}
                                        <span id="password_confirmation_error" class="color-w" style="color:red;">{{$errors->first('password_confirmation')}}</span>
                                    </div>
                                    <button class="btn btn-primary btn-block mb-20" type="submit">Change Password</button>
                                    <p class="text-right"><a href="{{ route('stores.index') }}">Back to dashboard</a></p>
                                {{Form::close()}}
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="passcss">
                <ul>
                    <li>Password should be a minimum of 16 characters</li>
                    <li>Password must consist of at least 1 character in uppercase</li>
                    <li>Password must consist of at least 1 numeric</li>
                    <li>Password must consist of at least 1 special character</li>
                </ul>
            </div> -->
        </div>
        <!-- /Main Content -->
    </div>
    <!-- /HK Wrapper -->
    {{ Html::script('backend/js/jquery.min.js',[]) }}
    <!-- Bootstrap Core JavaScript -->
    {{ Html::script('backend/js/popper.min.js',[]) }}
    {{ Html::script('backend/js/bootstrap.min.js',[]) }}
    <!-- Slimscroll JavaScript -->
    {{ Html::script('backend/js/jquery.slimscroll.js',[]) }}
    {{ Html::script('backend/js/dropdown-bootstrap-extended.js',[]) }}
    <!-- FeatherIcons JavaScript -->
    {{ Html::script('backend/js/feather.min.js',[]) }}
    {{ Html::script('backend/js/init.js',[]) }}
    {{ Html::script('backend/js/toster.min.js',[]) }}

</body> 
<script type="text/javascript">
    if("{{\Session::has('pwd_status')}}"){
       toastr.success("{{\Session::get('pwd_status')}}");
    }
    function validate(){
        $("[id$='_error']").empty();

        var current_password = $('#current_password').val();
        var password = $('#password').val();
        var password_confirmation = $('#password_confirmation').val();
        var msg = '';
        var regex = /^.*(?=.{4,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%*!&]).*$/;

        if (current_password == "") {
            msg = 'The current password field is required.';
            $('#current_password_error').text(msg);
        }

        if (password == "") {
            msg = 'The password field is required.';
            $('#password_error').text(msg);
        }else if(!regex.test(password)) {
            msg = 'The password field must be alphanumeric with minimum 4 characters.';
            $('#password_error').text(msg);
        }

        if (password_confirmation == "") {
            msg = 'The password confirmation field is required.';
            $('#password_confirmation_error').text(msg);
        }else if (!regex.test(password_confirmation)) {
            msg = 'The password confirmation field must be alphanumeric with minimum 4 characters.';
            $('#password_confirmation_error').text(msg);
        }else if (password != password_confirmation) {
            msg = 'Password did not match. Please try again.'; 
            $('#password_confirmation_error').text(msg);
        } 
        
        if(msg){
            return false;
        }
        return true;
    }
</script>
  @include('admin.layout.alert')
</html>