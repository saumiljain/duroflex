@extends('admin.layout.layout')

@section('header')
    <nav class="navbar navbar-expand-xl navbar-light fixed-top hk-navbar">
        <a id="navbar_toggle_btn" class="navbar-toggle-btn nav-link-hover" href="javascript:void(0);"><span class="feather-icon"><i data-feather="menu"></i></span></a>
        <div class="hk-pg-header col-md-6">
            <h4 class="hk-pg-title"><span class="pg-title-icon"><span class="feather-icon"></span></span>Create Store</h4>
        </div>
        
    </nav>
@include('admin.layout.overlay')
@stop
@section('content')
   <div class="container">
    <div class="row">
        <div class="col-xl-12">
            <section class="hk-sec-wrapper">
                <h5 class="hk-sec-title">Create Store</h5>
                <hr>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-sm">
                            <form method="POST" action="https://sms.duroflexworld.com/duroflex/public/admin/stores" >
                                @csrf
                            <div class="form-group row">
                                <label class="col-xl-3" for="name">Store Code<sup class="text-danger">*</sup></label>
                                <div class="col-sm-6">
                                   <input type="text" name="store_code" placeholder="Enter Store Code" class="form-control">
                                    <span id="order_number_error" class="help-inline text-danger">{{ $errors->first('store_code') }}</span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-xl-3" for="description">Store Coco<sup class="text-danger">*</sup></label>
                                <div class="col-sm-6">
                                    <input type="text" name="store_name" placeholder="Enter Store Coco" class="form-control" value="{{old('store_name')}}">
                                    <span id="reason_error" class="help-inline text-danger">{{ $errors->first('store_name') }}</span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-xl-3" for="description">Employee Name<sup class="text-danger">*</sup></label>
                                <div class="col-sm-6">
                                    <input type="text" name="employee_name" placeholder="Enter Employee Name" class="form-control" value="{{old('employee_name')}}">
                                    <span id="reason_error" class="help-inline text-danger">{{ $errors->first('employee_name') }}</span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-xl-3" for="description">Email Id<sup class="text-danger">*</sup></label>
                                <div class="col-sm-6">
                                    <input type="text" name="email" class="form-control" placeholder="Enter Email Id" value="{{old('email')}}">
                                    <span id="comment_error" class="help-inline text-danger">{{ $errors->first('email') }}</span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-xl-3" for="description">Mobile Number<sup class="text-danger">*</sup></label>
                                <div class="col-sm-6">
                                    <input type="text" name="mobile_no" class="form-control" placeholder="Enter Mobile Number" value="{{old('mobile_no')}}">
                                    <span id="comment_error" class="help-inline text-danger">{{ $errors->first('mobile_no') }}</span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-xl-3" for="description">Password<sup class="text-danger">*</sup></label>
                                <div class="col-sm-6">
                                    <input type="password" name="password" class="form-control" placeholder="Enter Password">
                                    <span id="comment_error" class="help-inline text-danger">{{ $errors->first('password') }}</span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-xl-3" for="description">Address<sup class="text-danger">*</sup></label>
                                <div class="col-sm-6">
                                    <textarea name="address" class="form-control" placeholder="Enter Address">{{old('address')}}</textarea>
                                    <span id="comment_error" class="help-inline text-danger">{{ $errors->first('address') }}</span>
                                </div>
                            </div>
<!--                             <div class="form-group row">
                                <label class="col-xl-3" for="description">Country<sup class="text-danger">*</sup></label>
                                <div class="col-sm-6">
                                    <input type="text" name="address" class="form-control" placeholder="Enter Country">
                                    <span id="comment_error" class="help-inline text-danger">{{ $errors->first('country') }}</span>
                                </div>
                            </div> -->
                            <div class="form-group row">
                                <label class="col-xl-3" for="description">State<sup class="text-danger">*</sup></label>
                                <div class="col-sm-6">
                                    <input type="text" name="state" class="form-control" placeholder="Enter State" value="{{old('state')}}">
                                    <span id="comment_error" class="help-inline text-danger">{{ $errors->first('state') }}</span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-xl-3" for="description">City<sup class="text-danger">*</sup></label>
                                <div class="col-sm-6">
                                    <input type="text" name="city" class="form-control" placeholder="Enter City" value="{{old('city')}}">
                                    <span id="comment_error" class="help-inline text-danger">{{ $errors->first('city') }}</span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-xl-3" for="description">Pincode<sup class="text-danger">*</sup></label>
                                <div class="col-sm-6">
                                    <input type="text" name="pincode" class="form-control" placeholder="Enter Pincode" value="{{old('pincode')}}">
                                    <span id="comment_error" class="help-inline text-danger">{{ $errors->first('pincode') }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="float-right">
                
                <input type="submit"  class="btn btn-sm btn-primary mr-10" value="Add Store">
             </form>   
            </div>
            <br>
            </section>
            
            
        </div>
    </div>
</div> 
@stop

@section('script')
@stop
@include('admin.layout.alert')
