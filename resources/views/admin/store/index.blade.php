@extends('admin.layout.layout')
@section('style')
{{ Html::style('backend/css/select2.min.css',[],IS_SECURE) }}
@stop
@section('header')
<form id="filter" action="" method="">
    <input type="hidden" name="type_name" value="{{Request::get('type')}}">
    {{ csrf_field()}}
    <nav class="navbar navbar-expand-xl navbar-light fixed-top hk-navbar">
        <a id="navbar_toggle_btn" class="navbar-toggle-btn nav-link-hover" href="javascript:void(0);"><span
                class="feather-icon"><i data-feather="menu"></i></span></a>
        <div class="container">
            <div class="hk-pg-header col-md-6">
                <h4 class="hk-pg-title"><span class="pg-title-icon"><span
                            class="feather-icon"></span></span>Store-Master</h4>
            </div>
            <div class="text-align-right col-md-6">

            </div>
        </div>
    </nav>
</form>
@stop
@section('content')
<div class="container">
    <div class="row">
        <div class="col-xl-12">
            <ul class="nav nav-tabs">
                <li class="@if(Request::get('type') == 'all') active @endif">
                    <a href="{{route('stores.index',['id'=>Request::get('id'),'type'=>'all'])}}">All <span
                            class="badge bg-success total_pincode"></span></a>
                </li>
                <li class="@if(Request::get('type') == 'active') active @endif">
                    <a href="{{route('stores.index',['id'=>Request::get('id'),'type'=>'active'])}}">Active <span
                            class="badge bg-success total_active_pincode"></span></a>
                </li>
                <li class="@if(Request::get('type') == 'inactive') active @endif">
                    <a href="{{route('stores.index',['id'=>Request::get('id'),'type'=>'inactive'])}}">Inactive <span
                            class="badge bg-warning total_inactive_pincode"></span></a>
                </li>
              <a href="<?= route('stores.create') ?>" class="btn btn-primary btn-sm mr-2 buttonOverAnimation" title="Add Store" style="margin-left: 700px;">
            <div data-text="Add Store" > <i class="fas fa-plus"></i>Create Store</div></a>
            </ul>
            <section class="hk-sec-wrapper">
                <div class="row">
                    <div class="col-sm">
                        <div class="table-wrap">
                            <table id="master_table" class="table table-hover w-100 display pb-30">
                                <thead>
                                    <tr>
                                        <th class="tdcheckbox"></th>
                                        <th class="tdstorecode">Store Code</th>
                                        <th class="tdcustomer">Store Coco</th>
                                        <th class="tdsonycenter">Email</th>
                                        <th class="tdsonycenter">Mobile No</th>
                                        <th class="tdstatus">Status</th>
                                        <th class="tdaction">Action</th>
                                        
                                        
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<!-- displaying import modal-->

@stop

@section('script')
{{  Html::script('backend/js/jquery.form.min.js', [],IS_SECURE) }}
{{  Html::script('backend/js/moment.min.js', [],IS_SECURE) }}
{{  Html::script('backend/js/daterangepicker.js', [],IS_SECURE) }}
{{  Html::script('backend/js/jquery.dataTables.min.js',[],IS_SECURE)  }}
{{  Html::script('backend/js/dataTables.bootstrap4.min.js',[],IS_SECURE)  }}
{{  Html::script('backend/js/dataTables.dataTables.min.js',[],IS_SECURE)  }}
{{  Html::script('backend/js/fnstandrow.js', [],IS_SECURE) }}
{{  Html::script('backend/js/nprogress.js',[],IS_SECURE)  }}
{{  Html::script('backend/js/handlebars-v4.0.12.js',[],IS_SECURE)  }}

<!-- AJAX FOR DATA TABLE  START-->

<script type="text/javascript">



    var status_type = "{{ Request::get('type') }}";
    if (status_type == 'inactive') {
        
        var url = "{{ URL::route('stores.index',['report_id'=>Request::get('id'),'type'=>'inactive'])}}";
        var url = "https://sms.duroflexworld.com/duroflex/public/admin/stores?type=inactive";
    }
    else if (status_type == 'active') {
        var url = "{{URL::route('stores.index',['report_id'=>Request::get('id'),'type'=>'active'])}}";
        var url = "https://sms.duroflexworld.com/duroflex/public/admin/stores?type=active";

    }
    else {
        var url = "{{URL::route('stores.index',['report_id'=>Request::get('id'),'type'=>'all'])}}";
	var url = "https://sms.duroflexworld.com/duroflex/public/admin/stores?type=all";

    }

    console.log(url);


        $(function(){ 
        var store_tbl = $('#master_table').dataTable({
        "bProcessing": false,
        "bServerSide": true,
        "autoWidth": true,
        "aaSorting": [
            [0, "desc"]
        ],
        lengthMenu: [
            [50, 100, 200, 500],
            ['50', '100', '200', '500']
        ],

        "sAjaxSource": url,
        "aoColumns": [
            { "mData": "store_id", bSortable: true, bVisible: false },
            {"mData":"store_code",bSortable:true, "className":"tdstorecode"},            
            { "mData": "store_name", "className": "tdcustomer", bSortable: true },
            { "mData": "email", "className": "tdcustomer", bSortable: true },
            { "mData": "mobile_no", "className": "tdcustomer", bSortable: true },

            {
                "mData": "status", "className": "tdstatus", bSortable: false,
                mRender: function (v, t, o) {
                    if (v == 1) {
                        return '<span class="badge badge-success">Active</span>';
                    } else {
                        return '<span class="badge badge-danger">Inactive</span>';
                    }
                }
            },

            {
                "mData": "store_id",
                bSortable: false,
                "className": "tdactions",
                sClass: "text-center",
                mRender: function (v, t, o) {
                var store_id = o['store_id'];
                
                var edit_urls = "stores/"+store_id+"/edit";
                var edit_url = "https://sms.duroflexworld.com/duroflex/public/admin/stores/"+store_id+"/edit";
		
                var act_html = "<div class='btn-group'>"
                    + "<a href=" + edit_url + " title='Edit Store' data-tool-tips='Edit Store' class='btn btn-xs btn-default' style='font-size:15px; line-height:9px;'><i class='glyphicon glyphicon-edit' style='color:#6f7a7f;'></i></a>"
                    + "</div>"
                return act_html;
            }
        },
            

            
        ],
    });
});

</script>
<script>
    
  @if(Session::has('error'))
  toastr.options =
  {
    "closeButton" : true,
    "progressBar" : true
  }
        toastr.error("{{ session('error') }}");
  @endif

  @if(Session::has('success'))
  toastr.options =
  {
    "closeButton" : true,
    "progressBar" : true
  }
        toastr.success("{{ session('success') }}");
  @endif

</script>

@stop



