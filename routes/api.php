<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StoreLoginController;
use App\Http\Controllers\API\StorOrderUpdateTagController;
use App\Http\Controllers\API\SendNotification;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', [StoreLoginController::class, 'login']);

Route::post('test',[StoreLoginController::class, 'test']);

Route::post('logout',[StoreLoginController::class,'logout']);

Route::post('store-order-tag-update',[StorOrderUpdateTagController::class, 'orderTagUpdate']);

Route::post('whatsapp-notification-send',[SendNotification::class,'sendwhatsappNotification']);
