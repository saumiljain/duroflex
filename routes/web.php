<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\Auth\LoginController;
use App\Http\Controllers\Admin\PasswordController;
use App\Http\Controllers\Admin\StoreController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {     return view('welcome'); })->middleware(['auth.shopify'])->name('home');

Route::get('/welcome', function () {
    return view('welcome');
})->name('welcome');

Route::get('/test',function(){
dd('hii');
});
Auth::routes();

Route::get('/admin/login', [LoginController::class, 'showLoginForm'])->middleware('guest')->name('admin.login');
Route::post('/admin/login-post', [LoginController::class, 'login'])->name('admin.login.post');
Route::get('/admin/logout', [LoginController::class, 'logout'])->name('admin.logout');

Route::middleware('admin')->namespace('Admin')->prefix('admin')->group( function() {

    Route::resource('stores','\App\Http\Controllers\Admin\StoreController',['except'=>['show','update']]);
    Route::post('stores/update',[StoreController::class,'updateData'])->name('stores.update');

    Route::get('/password/change',[PasswordController::class, 'changed'])->name('password.changed');
                
    Route::post('password/post_changed', [PasswordController::class, 'postchanged'])->name('password.post.changed'); 
});
            
